$(function(){

        $("[data-toggle='tooltip']").tooltip();
        $("[data-toggle='popover']").popover();
        $('.carousel').carousel({
            interval: 2000
        });
        $('#exampleModal').on('show.bs.modal', function(e){
            
          $('#contactoBtn').removeClass('btn-outline-success');
          $('#contactoBtn').addClass('btn-primary');
          $('#contactoBtn').prop('disable', true);

        });

        $('#exampleModal').on('hidden.bs.modal',function(e){

          $('#contactoBtn').prop('disable', false);

        });
      });